#ifndef __FUNCS_H_
#define __FUNCS_H_

#include "includes.h"

#include <sstream>
#include <fstream>
#include <string>
#include <vector>

template<typename T>
inline T fromString( const std::string &s)
{
    std::istringstream is(s);
    T t;
    is >> t;
    return t;
}

class Buff
{
	std::vector<char> mBuffer;

    inline void writeToBuffer(const char* data, int bytes)
    {
        for ( int i = 0; i < bytes; i++ )
			mBuffer.push_back(data[i]);
	}

public:
    Buff(size_t size)
    {
        mBuffer.reserve(size);
    }

    inline char getByte(int index) const
    {
		return mBuffer[index];
	}

    inline void clear()
    {
        mBuffer.clear();
    }

    inline size_t getSize()
    {
        return mBuffer.size();
    }

    inline void writeType(const char* d)
    {
        writeToBuffer(d, 4);
    }

    void writeData(const std::string &d)
    {
        size_t len = d.length()+1; //+1 for thew nill
        writeToBuffer((char*)&len, 4);
		writeToBuffer(d.c_str(), (int)d.length());

		len = 0;
		writeToBuffer((char*)&len, 1);
	}

    inline void writeData(const long d)
    {
		long len = 4;
		writeToBuffer((char*)&len, 4);
		writeToBuffer((char*)&d, 4);
	}

    inline void writeData(const float d)
    {
		long len = 4;
		writeToBuffer((char*)&len, 4);
		writeToBuffer((char*)&d, 4);
	}

    inline void writeRaw(const std::string &d)
    {
        writeToBuffer(d.c_str(), (int)d.length()-1);
    }

    inline void writeRaw(const long d)
    {
        writeToBuffer((char*)&d, 4);
    }

    inline void writeRaw(const float d)
    {
        writeToBuffer((char*)&d, 4);
    }

};

void fileWriteBuff(Buff* buff, std::ofstream& ofs);
void buffWriteCellStart(Buff* buff, const std::string& name, long flags, long x, long y, const std::string& rgn, long col);
void fileWriteStatData(std::ofstream& ofs,const std::string& type, const std::string& id, const std::string& mesh, const std::string& name, const std::string& script);
void buffWriteObjData(Buff* buff, long frmr, const std::string& id, float scale, float px, float py, float pz, float rx, float ry, float rz  );
void fileWriteEspHdr(std::ofstream& ofs);
void fileWriteCellHdr(Buff* buff, std::ofstream& ofs);

float getRand(float min, float max);

#endif //__FUNCS_H_

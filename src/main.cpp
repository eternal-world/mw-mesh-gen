long gNumRecords = 0;
long gNumRecordPos = -1;
#define PI 3.14159265
const double FROM_DEG = 180/PI;

#include "GenThread.h"

int main(int argc, char **argv)
{
    GenThread thread("Grass.esp", "GRS_", "Grass.ini", {"Data Files/Morrowind.esm"}, -1);
    return thread.start();
}
